﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameManager : MonoBehaviour {
	
	public Dictionary<string, AudioSource> AudioSources;
	public Canvas inGameCanvas; //canvas yang ada di game, button kecil pause itu
	public Canvas pauseCanvas; //canvas yang muncul saat game di pause
	public Canvas dieCanvas; //Canvas saat player mati
	public Canvas winCanvas; //canvas saat player menang
	public int level; //pemain sudah bermain sampai level berapa berdasarkan index pada build setting
	//public static GameManager control;

	private string statusCanvas; //status yang menyatakan canvas mana yg sedang aktif: {inGame, pause, die, and win Canvas}
	private int currentLevel; //level sekarang yang diindikasikan dengan index pada build setting
	private int nextLevel;
	private int menuLevel; //menu keberapa berdasarkan level scene yang sedang aktif, berpengaruh terdapat pemilihan menu
	private int maxLevel;
	private int sumMenu; //berapa banyak menu berdasarkan maxLevel
	/*
	void Awake(){
		if (control == null) {
			DontDestroyOnLoad (gameObject);
			control = this;
		} else if (control != this) {
			Destroy (gameObject);
		}
	}
	*/
	public int GetCurrentLevel(){
		return currentLevel;
	}

	public int GetNextLevel(){
		return nextLevel;
	}

	public int SumMenu(){
		return sumMenu;
	}

	public void SetStatusCanvas(string s){
		this.statusCanvas = s;
	}

	void Awake () {
		currentLevel = SceneManager.GetActiveScene().buildIndex; //ambil index dari scene di build setting
		maxLevel = SceneManager.sceneCountInBuildSettings;
		if (currentLevel == maxLevel) {
			nextLevel = currentLevel; //jika current level itu level maksimum, maka next level itu tetap di current level
		} else {
			nextLevel = currentLevel + 1;  
		}
		sumMenu =  maxLevel / 10 + 2;
		menuLevel = MenuLevel(currentLevel-sumMenu);
		/*
		inGameCanvas = GameObject.Find("InGameCanvas").GetComponent<Canvas>();
		pauseCanvas = GameObject.Find("PauseCanvas").GetComponent<Canvas>();
		dieCanvas = GameObject.Find("DieCanvas").GetComponent<Canvas>();
		winCanvas = GameObject.Find("WinCanvas").GetComponent<Canvas>();
		*/
		if (AudioSources == null) {
			AudioSources = new Dictionary<string, AudioSource> ();

			AudioSource[] source = GameObject.Find("SFX").GetComponentsInChildren<AudioSource>();

			for(int i = 0; i<source.Length; i++){
				AudioSources[source[i].gameObject.name] = source[i];
				//DontDestroyOnLoad(source[i]);
			}
		}
		dieCanvas.enabled = false;
		winCanvas.enabled = false;
        pauseCanvas.enabled = false;
		inGameCanvas.enabled = true;
        Time.timeScale = 1.0F;
		Load ();
	}

	void Start(){
		if (currentLevel >= 6) {
			statusCanvas = "inGame";
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.Escape)) {
			if (statusCanvas == "inGame") {
				Pause ();
			} else if (statusCanvas == "pause") {
				Resume ();
			} else if (statusCanvas == "die" || statusCanvas == "win") {
				Menu ();
			}
		}
	}

    int MenuLevel(int level) //menu keberapa berdasarkan level
	{
		if (level <= 10) {
			return 3;
		} else if (level > 10 && level <= 20) {
			return 4;
		} else if (level > 20 && level <= 30) {
			return 5;
		} else if (level > 30 && level <= 40) {
			return 6;
		} else if (level > 40 && level <= 50) {
			return 7;
		} else {
			return -1;
		}
	}

	IEnumerator waitTime(){
		yield return new WaitForSeconds (1);
	}

    public void Restart()
    {
        SceneManager.LoadScene(currentLevel);
    }

    public void Resume()
    {
        Time.timeScale = 1.0F;
        inGameCanvas.enabled = true;
        pauseCanvas.enabled = false;
		statusCanvas = "inGame";
    }

    public void Menu()
    {
        SceneManager.LoadScene(menuLevel);
    }

    public void Pause()
    {
        Time.timeScale = 0.0F;
		pauseCanvas.enabled = true;
        //.enabled = false;
		statusCanvas = "pause";
    }

	public void NextLevel(){
		inGameCanvas.enabled = false;
		if (currentLevel == maxLevel - 1) {
			Menu ();
		} else {
			SceneManager.LoadScene (nextLevel);
		}
	}

	public void Save(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/level.dat");
		LevelData data = new LevelData(); //membuat objek baru untuk di save
		data.setLevel (level);

		bf.Serialize (file, data);
		file.Close ();
	}

	public void Load(){

		if (File.Exists (Application.persistentDataPath + "/level.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/level.dat", FileMode.Open);
			LevelData data = (LevelData)bf.Deserialize (file);
			file.Close ();

			level = data.getLevel ();
		} else {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Create (Application.persistentDataPath + "/level.dat");
			LevelData data = new LevelData ();
			data.setLevel (8);
			level = 8;
			bf.Serialize (file, data);
			file.Close ();
		}
	}
}

//memakai kelas lain sebagai kelas kosong yang hanya berisi data yg ingin di save
[Serializable] //serializable agar file bisa ditulis dalam bentuk binary
class LevelData{
	private int currentLevel;  //data level yang ingin di save

	public void setLevel(int level){
		this.currentLevel = level;
	}

	public int getLevel(){
		return currentLevel;
	}
}