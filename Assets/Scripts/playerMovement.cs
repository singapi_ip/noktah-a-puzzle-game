using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class playerMovement : MonoBehaviour {
	public float speed;
	public float jumpForce;
	public float boundaryFloor = -2f; //jarak berapa sudah dinyatakan sebagai floor
	public int h;
	public float mini; //constraint untuk persentasi kecepatan minimum
	public Transform groundCheck;
	public LayerMask whatIsGround;

	bool grounded = false;
	bool sfxVolume;

	GameManager gm;
	Rigidbody2D dot;
	float groundCheckRadius = 0.2f;
	bool isColliding; //untuk mengecek apakah sudah collide apa belum, untuk menghindari collision ganda pada saat bersamaan
	bool groundedFirst = true; //untuk mengecek apakah pertama kali di ground atau tidak
	
	// Use this for initialization
	void Start() {
		dot = GetComponent<Rigidbody2D>();
		dot.velocity = new Vector2(h*speed, 0);
		gm = GameObject.Find ("Game Manager").GetComponent<GameManager> ();
		sfxVolume = GameObject.Find ("Background Music").GetComponent<BackgroundMusic> ().sfxVolume;
	}

	void FixedUpdate() {
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
		isColliding = false;

		if (grounded && !groundedFirst && transform.position.y < boundaryFloor) {
			if(sfxVolume)
				gm.AudioSources ["HitWall"].Play ();
			groundedFirst = true;
		}

		if (grounded && Input.GetButton ("Fire1")) {			
			if (EventSystem.current.IsPointerOverGameObject ()) {
				return;
			}
			Vector2 velocity = dot.velocity;
			velocity.y = jumpForce;
			dot.velocity = velocity;
			//dot.AddForce(transform.up * jumpForce);
			groundedFirst = false;
			if(sfxVolume)
				gm.AudioSources ["Jump"].Play ();
		}

		/*
		if (grounded) {
			dot.velocity = new Vector2(h*speed, dot.velocity.y);
		}
		*/
		if (Mathf.Abs (dot.velocity.x) > speed) {
			float kali = 1;
			if (dot.velocity.x < 0)
				kali = -1; 
			dot.velocity = new Vector2 (kali * speed, dot.velocity.y);
		}

		if (Mathf.Abs (dot.velocity.x) < mini * speed) {
			float kali = 1;
			if (dot.velocity.x < 0)
				kali = -1; 
			dot.velocity = new Vector2 (kali * mini * speed, dot.velocity.y);
		}
		dot.freezeRotation = true;
		//Debug.Log (dot.velocity);

	}
	
	void Flip() {
		Vector2 xScale = transform.localScale;
		xScale.x *= -1;
		transform.localScale = xScale;
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.CompareTag ("target")) {
			Time.timeScale = 0.0F;
			if(sfxVolume)
				gm.AudioSources ["OnTarget"].Play ();
			if (gm.GetNextLevel () > gm.level) {
				gm.level = gm.GetNextLevel ();
				gm.Save ();
			}
			gm.winCanvas.enabled = true;
			gm.inGameCanvas.enabled = false;
			Destroy (gameObject);
			gm.SetStatusCanvas ("win");
		} else if (other.gameObject.CompareTag ("spike")) {
			Time.timeScale = 0.0F;
			if(sfxVolume)
				gm.AudioSources ["HitSpike"].Play ();
			gm.dieCanvas.enabled = true;
			gm.inGameCanvas.enabled = false;
			Destroy (gameObject);
			gm.SetStatusCanvas ("die");
			//SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		} 
	}
	void OnCollisionEnter2D(Collision2D other){
		if (isColliding)
			return;
		if (other.gameObject.CompareTag ("wall")) {
			if(sfxVolume)
				gm.AudioSources ["HitWall"].Play ();
			isColliding = true;
		}
	}

}
