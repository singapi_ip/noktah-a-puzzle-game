﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BackgroundMusic : MonoBehaviour {

	public static BackgroundMusic music;
	private Dictionary<string, AudioSource> AudioSources;
	int currentlevel;
	int sumMenu; //banyak menu yg ada
	string bgm;
	AudioSource[] source;
	public bool bgmVolume = true; //apakah bgm bersuara atau tidak
	public bool sfxVolume = true; //apakah sfx bersuara atau tidak
	Image bgmImage;
	Image sfxImage;

	void Awake(){
		if (music == null) {
			DontDestroyOnLoad (gameObject);
			music = this;
		} else if (music != this) {
			Destroy (gameObject);
		}
	}

	void Start(){
		sumMenu = SceneManager.sceneCountInBuildSettings / 10 + 2;
		if (AudioSources == null) {
			AudioSources = new Dictionary<string, AudioSource> ();

			source = GetComponentsInChildren<AudioSource> ();

			for (int i = 0; i < source.Length; i++) {
				AudioSources [source [i].gameObject.name] = source [i];
				//DontDestroyOnLoad(source[i]);
			}
		}
		bgmImage = GameObject.Find ("Canvas").transform.GetChild (2).GetComponent<Image> ();
		sfxImage = GameObject.Find ("Canvas").transform.GetChild (3).GetComponent<Image> ();
		BGMImage ();
		SFXImage ();
	}

	void Update(){
		currentlevel = SceneManager.GetActiveScene ().buildIndex;
		if (currentlevel == 1) {
			if (bgmImage == null) {
				bgmImage = GameObject.Find ("Canvas").transform.GetChild (2).GetComponent<Image> ();
				sfxImage = GameObject.Find ("Canvas").transform.GetChild (3).GetComponent<Image> ();
			}
			BGMImage ();
			SFXImage ();
		}
		if (!bgmVolume) {
			StopBGMExcept ("");
			return;
		}
		if (currentlevel <= sumMenu) {
			StopBGMExcept ("Menu");
			if (AudioSources ["Menu"].isPlaying) {
				return;
			} else {
				AudioSources ["Menu"].Play ();
			}
		} else if (currentlevel - sumMenu <= 10) {
			StopBGMExcept ("Music 1");
			if (AudioSources ["Music 1"].isPlaying)
				return;
			else
				AudioSources ["Music 1"].Play ();
		} else if (currentlevel - sumMenu <= 20) {
			StopBGMExcept ("Music 2");
			if (AudioSources ["Music 2"].isPlaying)
				return;
			else
				AudioSources ["Music 2"].Play ();
		}else if (currentlevel - sumMenu <= 30) {
			StopBGMExcept ("Music 3");
			if (AudioSources ["Music 3"].isPlaying)
				return;
			else
				AudioSources ["Music 3"].Play ();
		}else if (currentlevel - sumMenu <= 40) {
			StopBGMExcept ("Music 4");
			if (AudioSources ["Music 4"].isPlaying)
				return;
			else
				AudioSources ["Music 4"].Play ();
		}else if (currentlevel - sumMenu <= 50) {
			StopBGMExcept ("Music 5");
			if (AudioSources ["Music 5"].isPlaying)
				return;
			else
				AudioSources ["Music 5"].Play ();
		}
	}

	void StopBGMExcept(string bgm){
		foreach(string key in AudioSources.Keys) {
			if (key == bgm)
				continue;
			else {
				if (AudioSources [key].isPlaying) 
					AudioSources [key].Stop ();
				else 
					continue;
			}				
		}
	}

	void BGMImage(){
		if (!bgmVolume) {
			bgmImage.color = Color.gray;
		} else {
			bgmImage.color = Color.white;
		}
	}

	void SFXImage(){
		if (!sfxVolume) {
			sfxImage.color = Color.gray;
		} else {
			sfxImage.color = Color.white;
		}
	}

	public void BGMVolume(){		
		bgmVolume = !bgmVolume;
		BGMImage ();
	}

	public void SFXVolume(){	
		sfxVolume = !sfxVolume;
		SFXImage ();
	}
}
