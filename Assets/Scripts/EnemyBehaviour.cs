﻿using UnityEngine;
using System.Collections;

public class EnemyBehaviour : MonoBehaviour {
    
    //public float speed;
    public Transform groundCheck;
    public LayerMask whatIsGround;

    private GameObject player;
    private Rigidbody2D playerRb;
    private Rigidbody2D rb;
    private float kali; //untuk pengali apakah dia bergerak ke kanan atau ke kiri
    private Vector2 temp; //untuk menyimpan nilai pengubah pada velocity

    float jumpForce = 5.5F;
    bool grounded = false;
    float groundCheckRadius = 0.2f;

    // Use this for initialization
    void Start () {
        player = GameObject.Find("dot");
        playerRb = player.GetComponent<Rigidbody2D>();
        rb = GetComponent<Rigidbody2D>();

        
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        kali = 1;
        if (rb.velocity.x < 0)
            kali = -1;

        //cek lagi apa dia di tanah apa tidak,
        //kalo player sudah di tanah, tapi musuh masih terbang dan player loncat lagi, maka musuh tidak loncat
        grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
        if (grounded && Input.GetButton("Jump"))
        {
            Vector2 velocity = rb.velocity;
            velocity.y = jumpForce;
            rb.velocity = velocity;
        }

        rb.velocity = new Vector2(kali * Mathf.Abs(playerRb.velocity.x), rb.velocity.y);
        rb.freezeRotation = true;
	}
}
