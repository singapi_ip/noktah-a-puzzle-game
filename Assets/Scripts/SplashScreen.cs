﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour {

	public float detik;

	void Start ()
	{
		StartCoroutine (Wait(detik));
	}

	IEnumerator Wait(float s){
		yield return new WaitForSeconds (s);
		SceneManager.LoadScene (1);
	}
}