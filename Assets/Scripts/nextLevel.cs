﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class nextLevel : MonoBehaviour {

	GameManager gm;

	void Start(){
		gm = GameObject.Find ("Game Manager").GetComponent<GameManager> ();
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag("Player")) {
			//SceneManager.LoadScene (gm.GetNextLevel());
			gm.winCanvas.enabled = true;
		}
	}

}