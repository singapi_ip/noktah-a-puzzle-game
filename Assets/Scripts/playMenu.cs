﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class playMenu : MonoBehaviour {

	void Update()
	{
		if (Input.GetKeyUp (KeyCode.Escape)) {
			if (SceneManager.GetActiveScene ().buildIndex == 1) {
				Application.Quit (); //Jika di home maka quit
			} else {
				SceneManager.LoadScene (1); //Jika di menu maka ke home
			}
		}
	}

	public void Menu()
    {
		SceneManager.LoadScene(1);
    }

	public void LoadScene(string levelname)
	{
		SceneManager.LoadScene (levelname);
	}


}
