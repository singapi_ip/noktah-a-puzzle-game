﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AddButtonOnClick : MonoBehaviour {
	//fungsi yang secara otomatis mencari gameobject dengan nama yang sama dan dimasukkan ke dalam button onlick
	//dalam kasus ini canvas pada home mencari Background Music dan dimasukkan ke dalam button BGM dan SFX

	Button bgm;
	Button sfx;
	GameObject  backgroundMusic;
	BackgroundMusic method;

	void Start () {
		bgm = gameObject.transform.GetChild (2).GetComponent<Button> ();
		sfx = gameObject.transform.GetChild (3).GetComponent<Button> ();
		backgroundMusic = GameObject.Find ("Background Music");
		method = backgroundMusic.GetComponent<BackgroundMusic> ();

		bgm.onClick.AddListener (delegate {
			method.BGMVolume ();
		});
		sfx.onClick.AddListener (delegate {
			method.SFXVolume ();
		});
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}