﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelEnabled : MonoBehaviour {

	public int levelPerRow;

	GameManager gm;
	int level;
	int sumMenu;
	Button temp;

	void Start(){
		gm = GameObject.Find ("Game Manager").GetComponent<GameManager> ();
		level = gm.level;
		sumMenu = gm.SumMenu ();

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < levelPerRow; j++) {
				temp = gameObject.transform.GetChild (0).GetChild (i).GetChild (j).gameObject.GetComponent<Button>();
				if (int.Parse(temp.name) <= level - sumMenu) {
					temp.interactable = true;
				}
				//Debug.Log ("Level: " + (level - sumMenu).ToString());
				//Debug.Log("nama button: " + temp.name);

			}
		}

	}
}