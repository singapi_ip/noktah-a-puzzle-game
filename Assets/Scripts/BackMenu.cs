﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class BackMenu : MonoBehaviour {
	
	void Update () {
		if (Input.GetKeyUp (KeyCode.Escape)) {
			SceneManager.LoadScene (1);
		}
	}
}