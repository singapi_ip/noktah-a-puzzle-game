﻿using UnityEngine;
using System.Collections;

public class targetRotate : MonoBehaviour {
	public float rotation = -3;
		
	// Update is called once per frame
	void FixedUpdate () {
		gameObject.transform.Rotate (new Vector3 (0, 0, 1), rotation);
	}

	void OnTriggerEnter2D(Collider2D col) {
		if(col.tag == "Player") {
			rotation = 0;
		}
	}
}
