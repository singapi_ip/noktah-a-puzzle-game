﻿using UnityEngine;
using System.Collections;

public class RotateTarget : MonoBehaviour {

	public float speed;

	void FixedUpdate(){
		transform.Rotate (Vector3.back * speed);
	}
}