﻿using UnityEngine;
using System.Collections;

public class ColliderTarget : MonoBehaviour {

	public RotateTarget Big;
	public RotateTarget Medium;
	public RotateTarget Small;

	void OnTriggerEnter2D(Collider2D other) {
		if(other.CompareTag("Player")) {
			Big.speed = 0;
			Medium.speed = 0;
			Small.speed = 0;

		}
	}
}