﻿using UnityEngine;
using System.Collections;

public class PlayerCondition : MonoBehaviour {

	int escapeInPause = 0;
	float timer = 0;

	void Update(){
		timer += Time.deltaTime;
		bool esc = Input.GetKeyUp (KeyCode.Escape);
		if (esc && escapeInPause == 0) {
			CanvasManager.cvm.PauseCanvasActive ();
			escapeInPause = 1;
		} else if (esc && escapeInPause == 1) {
			CanvasManager.cvm.PauseCanvasInActive ();
			escapeInPause = 0;
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.CompareTag ("target")) {
			SFXManager.sfx.PlayOnTarget ();
			CheckLevel ();
			CanvasManager.cvm.WinCanvasActive ();
			Destroy (gameObject);
		} else if (other.CompareTag ("spike")) {
			SFXManager.sfx.PlayHitSpike ();
			CanvasManager.cvm.DieCanvasActive ();
			Destroy (gameObject);
		} 
	}

	void OnCollisionEnter2D(Collision2D other){
		if (other.gameObject.CompareTag("wall") && timer >= 0.1f) {
			SFXManager.sfx.PlayHitWall ();
		}
	}

	void CheckLevel(){
		if (GameManagerScript.gm.GetActiveSceneIndex () + 1 > GameManagerScript.gm.levelPlay) {
			GameManagerScript.gm.levelPlay = GameManagerScript.gm.GetActiveSceneIndex () + 1;
			GameManagerScript.gm.Save ();
		}
	}
}