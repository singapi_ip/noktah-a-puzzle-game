﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {

	public float speed;
	public float jumpForce;
	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;

	Rigidbody2D rb;
	bool canJump;
	bool grounded = false;
	float timer;
	Vector2 destination;

	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		timer = 0;
	}

	void Update(){
		
		canJump = false;
		if (grounded && Input.GetButton("Fire1")) {
			canJump = true;
		}
		/*
		timer += Time.deltaTime;
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
		if (Mathf.Abs (rb.velocity.x) > speed || Mathf.Abs (rb.velocity.x) < speed) {
			Move ();
		}
		if (Input.GetKey(KeyCode.Mouse0) && grounded && timer >= 0.2f) {
			timer = 0;
			Jump ();
			SFXManager.sfx.PlayJump ();
		}
		*/
	}

	void FixedUpdate () {	
		timer += Time.deltaTime;
		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
		if (Mathf.Abs (rb.velocity.x) > speed || Mathf.Abs (rb.velocity.x) < speed) {
			Move ();
		}
		if (canJump && grounded && timer >= 0.2f) {
			timer = 0;
			Jump ();
			SFXManager.sfx.PlayJump ();
		}
	}

	void Move(){ 
		int kali = rb.velocity.x < 0 ? -1 : 1;
		Vector2 movement = new Vector2 (speed * kali, rb.velocity.y);
		rb.velocity = movement;
	}

	void Jump(){
		Vector2 newVelocity = new Vector2 (rb.velocity.x, jumpForce);
		rb.velocity = newVelocity;
	}
}