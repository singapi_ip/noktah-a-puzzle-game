﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour {

	public float detik;

	void Awake(){
		Application.targetFrameRate = 61;
	}

	void Start ()
	{
		StartCoroutine (Wait(detik));
	}

	IEnumerator Wait(float s){
		yield return new WaitForSeconds (s);
		GameManagerScript.gm.LoadSceneWithIndex (1);
	}

}