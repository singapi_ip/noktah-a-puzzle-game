﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour {

	int levelPlay;
	int thisLevel;
	Button but;

	void Start(){
		levelPlay = GameManagerScript.gm.levelPlay;
		but = GetComponent<Button> ();
		thisLevel = int.Parse (but.name);

		but.onClick.AddListener (delegate {
			GameManagerScript.gm.LoadSceneWithIndex (thisLevel + GameManagerScript.gm.GetIndexLevel1 () - 1);
		});

		if (thisLevel <= levelPlay - GameManagerScript.gm.GetIndexLevel1() + 1) {
			but.interactable = true;
		}
	}



}