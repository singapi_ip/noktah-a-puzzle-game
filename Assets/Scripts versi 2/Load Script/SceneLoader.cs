﻿using UnityEngine;
using System.Collections;

public class SceneLoader : MonoBehaviour {

	public void LoadSceneWithString(string scene){
		GameManagerScript.gm.LoadSceneWithString (scene);
	}

	public void LoadSceneWithIndex(int index){
		GameManagerScript.gm.LoadSceneWithIndex (index);
	}
}