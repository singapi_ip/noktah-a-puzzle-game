﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextTyper : MonoBehaviour {

	public float second;
	public GameObject[] go;
	public float speedFade;

	bool isShowing = false;
	Text[] text;
	Animator[] anim;

	void Awake(){
		Time.timeScale = 1.0F;
		text = new Text[go.Length];
		anim = new Animator[go.Length];
		for (int i = 0; i < go.Length; i++) {
			text[i] = go[i].GetComponent<Text>();
			anim [i] = go [i].GetComponent<Animator> ();
		}
	}

	void Start(){
		SFXManager.sfx.PlayEndingClap ();
		StartCoroutine (TypePerLine ());
	}

	void Update(){
		if (isShowing) {
			if (Input.GetKeyUp (KeyCode.Escape) || Input.GetButton ("Fire1")) {
				GameManagerScript.gm.LoadSceneWithIndex (1);
			}
		}
	}

	IEnumerator TypePerLine(){
		for (int i = 0; i < text.Length; i++) {		
			yield return new WaitForSeconds (second);	
			anim[i].SetBool("Fade", true);
		}
		yield return new WaitForSeconds (second - 1);	
		isShowing = true;
	}
}