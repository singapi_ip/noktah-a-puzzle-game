﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ResumeButton : MonoBehaviour {

	Button but;

	void Awake(){
		but = GetComponent<Button> ();
		but.onClick.AddListener (delegate {
			CanvasManager.cvm.PauseCanvasInActive ();
		});
	}
}