﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour {

	Button but;

	void Awake(){
		but = GetComponent<Button> ();

		but.onClick.AddListener (delegate {
			Time.timeScale = 1;
			GameManagerScript.gm.LoadSceneWithIndex (GetMenuIndex ());
		});
	}

	int GetMenuIndex(){
		return ((GameManagerScript.gm.GetActiveSceneIndex () - GameManagerScript.gm.GetIndexLevel1 ()) / 10) +  GameManagerScript.gm.CountAdditionalScene();
	}
}