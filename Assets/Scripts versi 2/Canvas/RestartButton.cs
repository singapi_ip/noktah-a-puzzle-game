﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class RestartButton : MonoBehaviour {

	Button but;

	void Awake(){
		but = GetComponent<Button> ();
		but.onClick.AddListener (delegate {
			Time.timeScale = 1;
			GameManagerScript.gm.LoadSceneWithIndex (GameManagerScript.gm.GetActiveSceneIndex ());
		});
	}
}