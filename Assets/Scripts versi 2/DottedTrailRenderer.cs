﻿using UnityEngine;
using System.Collections;

public class DottedTrailRenderer : MonoBehaviour {
	public GameObject dotLine;
	public float separatedTime;

	float timer = 0;

	void Update(){
		timer += Time.deltaTime;
		if (timer > separatedTime) {
			Instantiate (dotLine, transform.position, transform.rotation);
			timer = 0;
		}
	}


}