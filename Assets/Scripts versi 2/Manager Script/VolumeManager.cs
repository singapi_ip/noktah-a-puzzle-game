﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class VolumeManager : MonoBehaviour {
	/*	Fungsi untuk mengatur volume dari bgm dan sfx,
	 * 	apakah menggunakan bgm dan sfx atau tidak
	 */

	bool pressed;
	Button but;
	Image img;

	void Awake(){
		but = GetComponent<Button> ();
		img = GetComponent<Image> ();
	}

	void Start(){
		//button press berbanding terbalik dengan bgm volume. Jika bgm volume true, maka pressed false, vice versa
		pressed = !BackgroundMusicManager.bgm.GetBGMVolume ();
		img.color = pressed ? but.colors.disabledColor : but.colors.normalColor;
	}

	public void VolumeChange(){
		pressed = !pressed;
		img.color = pressed ? but.colors.disabledColor : but.colors.normalColor;
		BackgroundMusicManager.bgm.BgmVolumeChange ();
	}

}