﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class GameManagerScript : MonoBehaviour {

	public static GameManagerScript gm;
	public int countMenu;
	public int countAdditionalScene;
	public int levelPlay; //sudah sampai level berapa player memainkan game tersebut

	void Awake(){
		if (gm == null) {
			DontDestroyOnLoad (gameObject);
			gm = this;
		} else if (gm != this) {
			Destroy (gameObject);
		}
		Load ();
	}

	void Update(){
		if (Input.GetKeyUp (KeyCode.Escape) && GetActiveSceneIndex() == 1) {
			Application.Quit ();
		}
	}
		
	public int GetActiveSceneIndex(){
		return SceneManager.GetActiveScene ().buildIndex;
	}

	public string GetActiveSceneName(){
		return SceneManager.GetActiveScene ().name;
	}

	public int CountScene(){
		return SceneManager.sceneCountInBuildSettings;
	}

	public int CountMenu(){
		return countMenu; 
	}

	public int CountAdditionalScene(){
		return countAdditionalScene;
	}

	public int GetIndexLevel1(){
		return countMenu + countAdditionalScene;
	}

	public void LoadSceneWithIndex(int i){
		SceneManager.LoadScene (i);
	}

	public void LoadSceneWithString(string scene){
		SceneManager.LoadScene (scene);
	}
		
	public void Save(){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/level.dat");
		LevelData data = new LevelData (); //membuat objek baru untuk di save
		data.setLevel (levelPlay);

		bf.Serialize (file, data);
		file.Close ();
	}

	public void Load(){
		if (File.Exists (Application.persistentDataPath + "/level.dat")) {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/level.dat", FileMode.Open);
			LevelData data = (LevelData)bf.Deserialize (file);
			file.Close ();

			levelPlay = data.getLevel ();
		} else {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Create (Application.persistentDataPath + "/level.dat");
			LevelData data = new LevelData ();
			data.setLevel (8);
			levelPlay = 8;
			bf.Serialize (file, data);
			file.Close ();
		}
	}
}

[Serializable]
class LevelData{
	private int currentLevel;

	public void setLevel(int level){
		this.currentLevel = level;
	}

	public int getLevel(){
		return currentLevel;
	}
}