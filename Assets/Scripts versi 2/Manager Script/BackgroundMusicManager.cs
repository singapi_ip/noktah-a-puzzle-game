﻿using UnityEngine;
using System.Collections;

public class BackgroundMusicManager : MonoBehaviour {

	public static BackgroundMusicManager bgm;
	public AudioClip MenuMusic;
	public AudioClip Music1;
	public AudioClip Music2;
	public AudioClip Music3;
	public AudioClip Music4;
	public AudioClip Music5;

	AudioSource music;
	int indexlevel1;
	int additionalScene;
	bool bgmVolume;

	void Awake(){
		music = GetComponent<AudioSource> ();
		bgmVolume = true;
	}

	void Start () {
		if (bgm == null) {
			DontDestroyOnLoad (gameObject);
			bgm = this;
		} else if (bgm != this) {
			Destroy (gameObject);
		}
		indexlevel1 = GameManagerScript.gm.GetIndexLevel1();
		additionalScene = GameManagerScript.gm.CountAdditionalScene ();
	}

	void OnLevelWasLoaded(int index){
		if (index == 1 || index == GameManagerScript.gm.CountScene() - 1) {
			if (music.clip != MenuMusic)
				BgmLevelChange (MenuMusic);
		} else if (index >= additionalScene && index < indexlevel1) {
			if(music.clip != MenuMusic)
				BgmLevelChange (MenuMusic);
		} else if (index >= indexlevel1 && index < indexlevel1 + 10) {
			if(music.clip != Music1)
				BgmLevelChange (Music1);
		} else if (index >= indexlevel1 + 10 && index < indexlevel1 + 20) {
			if(music.clip != Music2)
				BgmLevelChange (Music2);
		} else if (index >= indexlevel1 + 20 && index < indexlevel1 + 30) {
			if(music.clip != Music3)
				BgmLevelChange (Music3);
		} else if (index >= indexlevel1 + 30 && index < indexlevel1 + 40) {
			if(music.clip != Music4)
				BgmLevelChange (Music4);
		} else if (index >= indexlevel1 + 40 && index < indexlevel1 + 50) {
			if(music.clip != Music5)
				BgmLevelChange (Music5);
		}
	}

	public bool GetBGMVolume(){
		return bgmVolume;
	}

	public void BgmVolumeChange(){
		bgmVolume = !bgmVolume;
		if (bgmVolume)
			music.Play ();
		else
			music.Stop ();
	}

	void BgmLevelChange(AudioClip m){
		if (bgmVolume) {
			music.Stop ();
			music.clip = m;
			music.Play ();
		}
	}
}