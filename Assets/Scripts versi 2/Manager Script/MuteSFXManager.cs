﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MuteSFXManager : MonoBehaviour {

	Button but;
	bool pressed;
	Image img;

	void Awake(){
		but = GetComponent<Button> ();
		img = GetComponent<Image> ();
	}

	void Start(){
		pressed = SFXManager.sfx.GetMuteState ();
		img.color = pressed ? but.colors.disabledColor : but.colors.normalColor;
	}

	public void SFXChange(){
		pressed = !pressed;
		img.color = pressed ? but.colors.disabledColor : but.colors.normalColor;
		SFXManager.sfx.ChangeMute ();
	}
}