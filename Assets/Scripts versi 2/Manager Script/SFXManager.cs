﻿using UnityEngine;
using System.Collections;

public class SFXManager : MonoBehaviour {

	public static SFXManager sfx;
	public AudioClip hitWall;
	public AudioClip hitSpike;
	public AudioClip jump;
	public AudioClip onTarget;
	public AudioClip endingClap;

	AudioSource sound;
	bool mute;

	void Awake(){
		if (sfx == null) {
			DontDestroyOnLoad (gameObject);
			sfx = this;
		} else if (sfx != this) {
			Destroy (gameObject);
		}

		sound = GetComponent<AudioSource> ();
		mute = false;
	}

	public void ChangeMute(){
		mute = !mute;
		sound.mute = mute;
	}

	public bool GetMuteState(){
		return mute;
	}

	public void PlayHitWall(){
		sound.PlayOneShot (hitWall, 0.5f);
	}

	public void PlayHitSpike(){
		sound.PlayOneShot (hitSpike, 0.5f);
	}

	public void PlayJump(){
		sound.PlayOneShot (jump, 0.3f);
	}

	public void PlayOnTarget(){
		sound.PlayOneShot (onTarget, 0.7f);
	}

	public void PlayEndingClap(){
		sound.PlayOneShot (endingClap, 1);
	}
}