﻿using UnityEngine;
using System.Collections;

public class CanvasManager : MonoBehaviour {

	public static CanvasManager cvm;
	public Canvas pauseCanvasIns;
	public Canvas dieCanvasIns;
	public Canvas winCanvasIns;

	string stateCanvas; //status untuk melihat canvas mana yang aktif
	Canvas pauseCanvas;
	Canvas dieCanvas;
	Canvas winCanvas;

	void Awake(){
		if (cvm == null) {
			DontDestroyOnLoad (gameObject);
			cvm = this;
		} else if (cvm != this) {
			Destroy (gameObject);
		}
	}

	public void PauseCanvasInActive(){
		disableAllCanvas ();
		Time.timeScale = 1;
	}

	public void PauseCanvasActive(){
		disableAllCanvas ();
		Time.timeScale = 0;
		pauseCanvas.enabled = true;
	}

	public void WinCanvasActive(){
		disableAllCanvas ();
		Time.timeScale = 0;
		winCanvas.enabled = true;
	}

	public void DieCanvasActive(){
		disableAllCanvas ();
		Time.timeScale = 0;
		dieCanvas.enabled = true;
	}

	void OnLevelWasLoaded(int index){
		if (index >= GameManagerScript.gm.GetIndexLevel1 () && index <= GameManagerScript.gm.CountScene () - 1) {
			if (!GameObject.Find ("PauseCanvas")) {
				pauseCanvas = (Canvas)Instantiate (pauseCanvasIns);
				dieCanvas = (Canvas)Instantiate (dieCanvasIns);
				winCanvas = (Canvas)Instantiate (winCanvasIns);
			}
		}
	}

	void disableAllCanvas(){
		pauseCanvas.enabled = false;
		dieCanvas.enabled = false;
		winCanvas.enabled = false;
	}
}