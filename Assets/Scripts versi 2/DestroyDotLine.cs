﻿using UnityEngine;
using System.Collections;

public class DestroyDotLine : MonoBehaviour {
	public float speedFade;
	public float scaleFade;

	void Update(){
		transform.localScale = transform.localScale - (transform.localScale * Time.deltaTime * speedFade);
		if (transform.localScale.x <= scaleFade) {
			Destroy (gameObject);
		}
	}

}