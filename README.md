## Noktah A Puzzle Game 
Noktah is a simple yet challenging puzzle game based on your precision.
The ball moves for itself. You can't control it's speed. You can't control it's movement. The only way to solve the puzzle is to jump, or not to jump.

Noktah - A Puzzle Game was made by two students during two months.

Version: 1.0.0
### Dependencies 
1. Unity 5.3.2f1 Personal

Link download: bit.ly/noktahapuzzlegame